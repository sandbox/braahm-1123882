quicktabs

=> create quicktabs programmatorically

=> in each tab you have a couple of views

=> each taxonomy term creates a tab, the term servers as a filter for the view

issue: in quicktabs you can't render a block with attachment => will only render the block
=> create a block programmorically which consist of multiple views
OR
=> create a quicktab programmoricaly which consist of multiple tabs with multiple views

ref: http://www.leveltendesign.com/blog/dustin-currie/how-programmatically-create-drupal-quicktab-block

type of quicktabs: http://drupal.org/node/332895
http://drupal.org/node/1058924


Obeservations:
1. to hide a block on the blocks listing pages, just omit the $op == 'list' operation

Issues:
1. after disabling the module and created an new 'Tabbed' vocab => views content didn't show up => needed to resave the view to salve it (caching?)

ToDo:
1. Warning when deleting the 'Tabbed' taxonmy